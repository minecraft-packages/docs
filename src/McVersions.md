# Minecraft Versions

## Definition

* **pattern** `{edition}{channel}{version}`
  * **edition**
    * `j` Java Edition (other editions are not supported at the moment)
  * **channel** [Read more](https://minecraft.gamepedia.com/Version_history)
    * `r` release / stable
    * `b` old beta
    * `a` old alpha
    * `s` developer snapchots
  * **version** see [available versions](mcVersions.json)
* **regex** `^j(([r|b|a]){1}(([0-9]{1,2})\.){1,3}[0-9]{1,2})|j([s][0-9]{2}w[0-9]{2}[a-zA-Z])$`
* **default** `jr`

## Examples

* [jr1.12.2](https://minecraft.gamepedia.com/1.12.2)
* [jr](https://minecraft.gamepedia.com/Java_Edition)
* [jb1.8](https://minecraft.gamepedia.com/Beta_1.8)
* [js18w09a](https://minecraft.gamepedia.com/18w09a)